So I updated the DESeq script new results can be found in ```/local/data/course/project/desesperados/results/gene_expression```. Old (wrong) results may be found in the ```.backup``` folder.

```
total 18M
-rw-rw-r-- 1 bakke333 domain users 2.7M Dec 12 17:09 DESseq_all_counts.csv
-rw-rw-r-- 1 bakke333 domain users  12M Dec 12 17:09 DESseq_test_table.csv
-rw-rw-r-- 1 bakke333 domain users  20K Dec 12 17:09 MA_Leaf53.png
-rw-rw-r-- 1 bakke333 domain users 8.2K Dec 12 17:09 MA_Leaf51.png
-rw-rw-r-- 1 bakke333 domain users  21K Dec 12 17:09 MA_Leaf177.png
-rw-rw-r-- 1 bakke333 domain users  18K Dec 12 17:09 MA_Leaf131.png
-rw-rw-r-- 1 bakke333 domain users  44K Dec 12 17:08 dispersion_estimates.png
-rw-rw-r-- 1 bakke333 domain users 8.9K Dec 12 17:08 distribution.png
-rw-rw-r-- 1 bakke333 domain users 3.2M Dec  8 09:04 gene_count_matrix.csv
```
