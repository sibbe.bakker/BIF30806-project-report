\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Importance and theory}{2}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Aim and research question}{2}{subsection.1.2}%
\contentsline {section}{\numberline {2}Methods}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Experimental design \& methods}{3}{subsection.2.1}%
\contentsline {paragraph}{Sample selection and treatment}{3}{section*.2}%
\contentsline {paragraph}{RNA sequencing}{3}{section*.3}%
\contentsline {subsection}{\numberline {2.2}Data analysis}{3}{subsection.2.2}%
\contentsline {paragraph}{Overview of the analysis}{3}{section*.4}%
\contentsline {paragraph}{Cleanup of \texttt {FASTQ} files}{3}{section*.5}%
\contentsline {paragraph}{Mapping RNA sequences to produce read counts}{3}{section*.6}%
\contentsline {paragraph}{Analysis of differentially expressed genes}{4}{section*.7}%
\contentsline {paragraph}{Analysis of differentially expressed genes}{4}{section*.8}%
\contentsline {paragraph}{Clustering analysis}{4}{section*.9}%
\contentsline {section}{\numberline {3}Results}{4}{section.3}%
\contentsline {paragraph}{Data quality}{4}{section*.10}%
\contentsline {paragraph}{Alignment to TAIR 10 reference genome}{4}{section*.11}%
\contentsline {paragraph}{Determination of \glspl {deg}}{5}{section*.13}%
\contentsline {paragraph}{The common transcriptional response to bacterial induced stunted growth}{5}{section*.15}%
\contentsline {section}{\numberline {4}Discussion}{6}{section.4}%
\contentsline {subsection}{\numberline {4.1}Immune responses elicited by commensals tested in this work.}{6}{subsection.4.1}%
\contentsline {paragraph}{Immune responses in \gls {Col} plants infected with commensal bacteria}{6}{section*.18}%
\contentsline {paragraph}{Relation to other works}{6}{section*.19}%
\contentsline {subsection}{\numberline {4.2}Recommendations for future work}{8}{subsection.4.2}%
\contentsline {paragraph}{Improvements to current work.}{8}{section*.20}%
\contentsline {paragraph}{Follow up research}{8}{section*.21}%
\contentsline {paragraph}{Concluding remarks}{9}{section*.22}%
