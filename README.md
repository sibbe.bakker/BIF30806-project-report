# `bif30806_final_report` repository top level readme

- Sibbe Bakker

![status not yet final](https://img.shields.io/badge/status-final-green)

## Description

In this repository, I have added the files I used in the creation of the bioinformatics final report. The data was analysed in colaboration with  Suraj Hassan Muralidhar, Laurens Klinkien,  Marjolein Relouw, and  Erika Castañeda. The shared gitlab repository can be found [here](https://git.wur.nl/desesperados/desperados_code_base.git), this is a private repository so you might not have access. If this is the case, please email me at [sibbe.bakker@wur.nl](mailto:sibbe.bakker@wur.nl).

## Files

```

.
├── data
│   ├── analysis
│   │   ├── gene_expression
│   │   │   ├── DEgenes_pvalue_0_01_log2FC_4.csv
│   │   │   ├── DESseq_all_counts.csv
│   │   │   ├── DESseq_test_table.csv
│   │   │   ├── dispersion_estimates.png
│   │   │   ├── distribution.png
│   │   │   ├── gene_count_matrix.csv
│   │   │   ├── MA_Leaf131.png
│   │   │   ├── MA_Leaf177.png
│   │   │   ├── MA_Leaf51.png
│   │   │   ├── MA_Leaf53.png
│   │   │   └── README.md
│   │   ├── GO_and_KEGG
│   │   │   ├── go_core-crop.pdf
│   │   │   ├── go_core.pdf
│   │   │   ├── GO_sibbe_code_131.csv
│   │   │   ├── GO_sibbe_code_177.csv
│   │   │   ├── GO_sibbe_code_51.csv
│   │   │   ├── GO_sibbe_code_53.csv
│   │   │   ├── GO_sibbe_code_all.csv
│   │   │   ├── kegg_results_all.csv
│   │   │   ├── plot_131_core.png
│   │   │   ├── plot_131_split.png
│   │   │   ├── plot_177_core.png
│   │   │   ├── plot_177_split.png
│   │   │   ├── plot_51_core.png
│   │   │   ├── plot_51_split.png
│   │   │   ├── plot_53_core.png
│   │   │   ├── plot_53_split.png
│   │   │   └── Rplots.pdf
│   │   ├── heatmaps
│   │   │   ├── core_cluster_reworked_by_laurens.jpeg
│   │   │   ├── core_gene_cluser.jpeg
│   │   │   ├── coregene+heatmaptrial.jpeg
│   │   │   ├── coregene+heatmaptrial.pdf
│   │   │   ├── core_heatmap.jpeg
│   │   │   ├── core_heatmap.pdf
│   │   │   ├── heatmap_all_genes.jpeg
│   │   │   ├── Heatmap_of_DEgenes-crop.pdf
│   │   │   ├── Heatmap_of_DEgenes.pdf
│   │   │   ├── Sample_Clustering_expressed_genes.jpeg
│   │   │   └── Sample_Clustering_of_DEgenes.jpeg
│   │   └── volcanos
│   │       ├── volcano_131.png
│   │       ├── volcano_177.png
│   │       ├── volcano_51.png
│   │       └── volcano_53.png
│   └── quality
│       ├── fastqc
│       │   ├── multiqc_data
│       │   │   ├── multiqc_citations.txt
│       │   │   ├── multiqc_data.json
│       │   │   ├── multiqc_fastqc.txt
│       │   │   ├── multiqc_general_stats.txt
│       │   │   ├── multiqc.log
│       │   │   └── multiqc_sources.txt
│       │   └── multiqc_report.html
│       └── hist2
│           ├── hisat2_se_plot.tsv
│           ├── multiqc_data
│           │   ├── multiqc_citations.txt
│           │   ├── multiqc_data.json
│           │   ├── multiqc_general_stats.txt
│           │   ├── multiqc_hisat2.txt
│           │   ├── multiqc.log
│           │   └── multiqc_sources.txt
│           └── multiqc_report.html
├── README.md
└── report
    ├── abstract.aux
    ├── inclusions
    │   ├── growth_morphology.jpeg
    │   └── hisat2_stats.png
    ├── report.aux
    ├── report.bbl
    ├── report.bcf
    ├── report.blg
    ├── report.fdb_latexmk
    ├── report.fls
    ├── report.log
    ├── report.out
    ├── report.pdf
    ├── report.run.xml
    ├── report.synctex.gz
    ├── report.tex
    └── report.toc

13 directories, 75 files
```
